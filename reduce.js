const items = [1, 2, 3, 4, 5, 5];

/*
Task

1.Do NOT use .reduce to complete this function.
2.How reduce works: A reduce function combines all elements into a
single value going from left to right.
3.Elements will be passed one by one into `cb` along with the `startingValue`.
4.`startingValue` should be the first argument passed to `cb` and the array
element should be the second argument.
5.`startingValue` is the starting value.  If `startingValue` is undefined
then make `elements[0]` the initial value.

*/

function helper(startingValue, ele) {
  return startingValue + ele;
}

function reduce(elements, cb, startingValue) {
  let sum = 0;
  for (ele of elements) {
    sum += cb(startingValue, ele);
  }

  return sum;
}

module.exports = { reduce, helper };

// console.log(reduce(items, helper, 0));
