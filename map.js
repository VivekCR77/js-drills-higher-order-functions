const items = [1, 2, 3, 4, 5, 5];

/*
Task

1.Do NOT use .map, to complete this function.
2.How map works: Map calls a provided callback function
once for each element in an array, in order, and functionructs
a new array from the res .
3.Produces a new array of values by mapping each value in list
through a transformation function (iteratee).
4.Return the new array.


*/

function iteratee(ele) {
  return ele * 3;
}

function map(elements, cb) {
  arr = [];
  for (ele of elements) {
    arr.push(cb(ele));
  }
  return arr;
}

module.exports = { map, iteratee };

// console.log(map(items, iteratee));
