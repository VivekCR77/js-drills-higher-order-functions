const items = [1, 2, 3, 4, 5, 5];

/*
Task

1.Do NOT use .includes, to complete this function.
2.Look through each value in `elements` and pass each element to `cb`.
3.If `cb` returns `true` then return that element.
4.Return `undefined` if no elements pass the truth test.
*/

function ispresent(ele, elements) {
  for (item of elements) {
    if (item === ele) return true;
  }

  return false;
}

function find(elements, cb) {
  if (cb(elements[0], elements)) return elements[0];
  return "undefined";
}

module.exports = { find, ispresent };

// console.log(find(items, ispresent));
