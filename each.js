const items = [1, 2, 3, 4, 5, 5];

/*
Task

1.Do NOT use forEach to complete this function.
2.Iterates over a list of elements, yielding each in turn to the `cb` function.
3.This only needs to work with arrays.
4.You should also pass the index into `cb` as the second argument
5.based off http://underscorejs.org/#each

*/

function hello(ele, index) {
  console.log(`At index ${index} -> ${ele}`);
}

function each(elements, cb) {
  for (index in elements) {
    cb(elements[index], index);
  }
}

module.exports = { each, hello };

// each(items, hello);
