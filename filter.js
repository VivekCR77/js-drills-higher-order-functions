const items = [1, 2, 3, 4, 5, 5];

/*
Task

1.Do NOT use .filter, to complete this function.
2.Similar to `find` but you will return an array of all elements that passed the truth test
3.Return an empty array if no elements pass the truth test
*/

function isOdd(ele) {
  if (!(ele % 2 == 0)) {
    return true;
  }
  return false;
}

function filter(elements, cb) {
  const odd_arr = [];

  for (ele of elements) {
    if (cb(ele)) odd_arr.push(ele);
  }

  if (odd_arr.length > 0) return odd_arr;

  return "undefined";
}

module.exports = { filter, isOdd };

// console.log(filter(items, isOdd));
