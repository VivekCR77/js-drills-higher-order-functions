const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

/* 
Task:

1.Flattens a nested array (the nesting can be to any depth).
2.Hint: You can solve this using recursion.
3.Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
*/

function flatten(elements) {
  const flattened_array = [];

  function helper(elements) {
    for (ele of elements) {
      if (Array.isArray(ele)) {
        helper(ele);
      } else {
        flattened_array.push(ele);
      }
    }
  }
  helper(elements);
  return flattened_array;
}

module.exports = { flatten };

// console.log(flatten(nestedArray));
